### Ar condicionado do carro

É muito interessante! O manual do meu carro diz para abaixar as janelas para que todo o ar quente saia antes de ligar o ar condicionado. POR QUE?

Não é de admirar que tem mais gente morrendo de câncer do que nunca. Queria saber de onde isso vem, mas aqui está um exemplo que explica muito dos incidentes que têm causado câncer.

Isso aconteceu recentemente com um rapaz na Prudente de Morais, que de ressaca, dormiu 24h seguidas dentro do carro com o ar-condicionado ligado e acordou com cancer.

Muitas pessoas estão nos seus carros a começar pela manhã, e por último a noite, 7 dias por semana.

Ler isso me faz sentir culpado e doente. Por favor passe essa informação para quantas pessoas for possível. Nunca é tarde demais para fazer algumas mudanças!

Por favor, NÃO ligue o ar condicionado assim que entrar no carro.

Abra as janelas ao entrar no carro e depois de alguns minutos, LIGUE o ar condicionado.

Aqui está o motivo: de acordo com pesquisas, o painel do carro, os assentos, dutos do ar condicionado, na verdade todos os objetos de plástico em seu veículo emitem Benzeno, uma toxina que causa câncer. Uma substância MUITO cancerígena. Separe um tempo para observar o cheiro de plástico aquecido em seu carro quando você abri-lo, e antes de ligar o motor.

Além de causar câncer, Benzeno intoxica os ossos, causa anemia e reduz as células brancas do sangue. Uma exposição prolongada pode causar leucemia e aumenta o risco de alguns tipos de câncer. Ele também pode causar aborto em mulheres grávidas.

O nível "aceitável" de Benzeno em lugares fechados é de 50mg por pé quadrado (144 polegadas quadradas, medida utilizada nos estados unidos).

Um carro estacionado em lugar fechado, com as janelas fechadas, irá conter de 400 a 800 mg de Benzeno - 8 vezes mais que o aceitável.

Se estacionado em lugares abertos ao Sol, numa temperatura de 16˚C (converti, o texto dizia 60˚F), o nível de benzeno vai para 2000 a 4000 mg, 40 vezes a mais que o aceitável.

Quem entra no carro mantendo as janelas fechadas, vai inalar quantidades excessivas de toxina Benzeno.

O Benzeno é uma toxina que afeta seus rins e fígado. E o pior, é extremamente difícil para o seu corpo para expulsar esse material tóxico.

Então amigos, por favor abram as janelas e portas do seu carro, dê um tempo para arejar o interior (dissipar o conteúdo mortal) antes de entrar no veículo.