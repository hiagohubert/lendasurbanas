### O Homem do Saco

Derivada dos mendigos que permeiam todas as cidades, essa lenda é usada pelas mães para assustar os meninos malcriados que saem para brincar sozinhos na rua. De acordo com ela, um velho malvestido, e com um enorme saco de pano nas costas, anda pela cidade levando embora as crianças que fazem “arte”. Em algumas versões, o velho é retratado realmente como um mendigo, outras ainda o apresentam como um cigano; creio que isso dependa da região do país onde ela é contada. Há ainda versões mais detalhadas (entendam como cruéis) em que o velho (mendigo ou cigano) leva a criança para sua casa e lá faz sabonetes e botões com elas.

Certa vez, foi achado um em Petropolis, com a barriga cheia de pedaços de pessoas, ele era canibal.